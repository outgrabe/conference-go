from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json

def get_photo(city, state):
    # Create a dictionary for the headers to use in the request
    url = f'https://api.pexels.com/v1/search?query={city}&{state}'
    headers = {'Authorization': PEXELS_API_KEY}
    response = requests.get(url, headers=headers)
    content = json.loads(response.text)
    # Create the URL for the request with the city and state
    # Make the request
    # Parse the JSON response
    # Return a dictionary that contains a `picture_url` key and
    return content["https://www.pexels.com/photo/photo-of-men-having-conversation-935949/"]
    #   one of the URLs for one of the pictures in the response


#def get_weather_data(city, state):
    # Create the URL for the geocoding API with the city and state
    # Make the request
    # Parse the JSON response
    # Get the latitude and longitude from the response

    # Create the URL for the current weather API with the latitude
    #   and longitude
    # Make the request
    # Parse the JSON response
    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    # Return the dictionary
